package ru.ser.book;

public class Demo {

    public static void main(String[] args) {

        Book book1 = new Book("Герц", "Боба и Бобоша", 1975);

        Book book2 = new Book("Есенин", "Что это такое", 1981);

        Book book3 = new Book("Жуковский", "Кот в сапогах", 1985);

        Book book4 = new Book("Флэйни", "Рай где-то рядом", 2018);

        Book book5 = new Book("Шаипов", "18ИТ18", 2019);

        Book[] books = {book1, book2, book3, book4, book5};

        yearsBook2018(books);

        sameYears(book1, book2);

    }

    private static void yearsBook2018(Book[] books) {

        for (Book book : books) {

            if (book.getYear() == 2018) {

                System.out.println(book);

            }

        }

    }

    private static void sameYears(Book book1, Book book2) {

        if (book1.isSameYears(book2)) {

            System.out.println("Книга(и) не относятся к данной категории ");

        } else {

            System.out.println("Книга(и) относятся к данной категории ");

        }

    }

}
